package com.epam.javaclasses.initialization;

import com.epam.javaclasses.comparator.ComparatorCriterion;
import com.epam.javaclasses.entity.Jewelry;
import com.epam.javaclasses.logger.LogWriter;

public class SecondTask {
    public static void main( String[] args ) {
        Jewelry jewelry = new Jewelry();                // Creating a jewelry
        jewelry.assembleJewelry(5);        // Adding gems to a jewelry
        System.out.println("Initial jewelry gems:");
        jewelry.printJewelryGems(jewelry.getJewelryGems());
        jewelry.printJewelryTotalCostAndWeight();       // Printing total weight and cost of a jewelry
        jewelry.sortJewelryGemsBy(ComparatorCriterion.COST);  // Sorting jewelry gems by cost
        System.out.println("Jewelry gems after sorting by cost:");
        jewelry.printJewelryGems(jewelry.getJewelryGems());
        jewelry.printGemWithNumericalParameter(ComparatorCriterion.CLARITY, 2, 6); // Looking for a gem with clarity between 2 and 6
        LogWriter.writeJewelryAssemblingLog("log.txt", jewelry);
    }
}
