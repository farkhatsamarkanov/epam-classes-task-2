package com.epam.javaclasses.factory;

public enum GemType {   // Types of gems
    DIAMOND,
    EMERALD,
    RUBY
}
