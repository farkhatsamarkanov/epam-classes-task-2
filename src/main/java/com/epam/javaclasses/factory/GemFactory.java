package com.epam.javaclasses.factory;

import com.epam.javaclasses.entity.*;

public class GemFactory {       // Factory pattern to build gems
    public static Gem getGemFromFactory(GemType type) {
        switch (type) {
            case EMERALD:
                return new Emerald("emerald", 100, 6.3, 1.15,"green");
            case DIAMOND:
                return new Diamond("diamond", 150.5, 3.6, 5.55,"white");
            case RUBY:
                return new Ruby("ruby", 85.6, 4.1, 3.12,"red");
            default:
                throw new IllegalArgumentException("Illegal type of gem!");
        }
    }
}
