package com.epam.javaclasses.comparator;

public enum ComparatorCriterion {   // Comparison criteria for sorting / finding specific gem
    TYPE,
    COST,
    WEIGHT,
    CLARITY,
    COLOR
}
