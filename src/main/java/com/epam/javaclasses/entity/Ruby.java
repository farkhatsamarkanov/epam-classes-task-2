package com.epam.javaclasses.entity;

public class Ruby extends Gem {
    public Ruby(String type, double cost, double weight, double clarity, String color) {
        super(type, cost, weight, clarity, color);
    }

    @Override
    public String cut() {
        return "Ruby is being cut";
    }
}
