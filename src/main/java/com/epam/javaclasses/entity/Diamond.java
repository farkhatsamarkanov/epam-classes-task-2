package com.epam.javaclasses.entity;

public class Diamond extends Gem {
    public Diamond(String type, double cost, double weight, double clarity, String color) {
        super(type, cost, weight, clarity, color);
    }

    @Override
    public String cut() {
        return "Diamond is being cut";
    }
}
