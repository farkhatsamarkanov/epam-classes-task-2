package com.epam.javaclasses.entity;

import java.util.Objects;

public abstract class Gem { // Abstract parent class  for all gems
    private String type;      // Class fields
    private double cost;
    private double weight;
    private double clarity;
    private String color;

    public Gem(String type, double cost, double weight, double clarity, String color) { // Constructor
        this.type = type;
        this.cost = cost;
        this.weight = weight;
        this.clarity = clarity;
        this.color = color;
    }

    public double getCost() {
        return cost;
    }                                               // Getters

    public double getWeight() {
        return weight;
    }

    public String getType() {
        return type;
    }

    public double getClarity() {
        return clarity;
    }

    public String getColor() {
        return color;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }                                 // Setters

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setClarity(double clarity) {
        this.clarity = clarity;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public abstract String cut();                                                           // Methods

    @Override
    public String toString() {
        return "Gem{" +
                "type='" + type + '\'' +
                ", cost=" + cost + '\'' +
                ", weight=" + weight + '\'' +
                ", clarity='" + clarity + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Gem)) return false;
        Gem gem = (Gem) o;
        return Double.compare(gem.getCost(), getCost()) == 0 &&
                Double.compare(gem.getWeight(), getWeight()) == 0 &&
                Double.compare(gem.getClarity(), getClarity()) == 0 &&
                Objects.equals(getType(), gem.getType()) &&
                Objects.equals(getColor(), gem.getColor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getCost(), getWeight(), getClarity(), getColor());
    }
}


