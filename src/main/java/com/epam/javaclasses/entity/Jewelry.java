package com.epam.javaclasses.entity;

import com.epam.javaclasses.factory.*;
import com.epam.javaclasses.comparator.ComparatorCriterion;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class Jewelry {
    private List<Gem> jewelryGems;                                                           // Class fields
    private StringBuilder log = new StringBuilder("com.epam.javaclasses.entity.Gem\n");

    public List<Gem> getJewelryGems() {
        return jewelryGems;
    }                               // Getters

    public StringBuilder getLog() {
        return log;
    }

    public void assembleJewelry (int numberOfGems) {                                        // Method to assemble jewelry with specific number of gems
        jewelryGems = new ArrayList<>();
        for (int i = 0; i < numberOfGems; i++) {
            Gem gem = GemFactory.getGemFromFactory(GemType.values()[new Random().nextInt(GemType.values().length)]);
            jewelryGems.add(gem);
            log.append(gem.cut())
               .append(" and added to jewelry\n");
        }
        log.append("Jewelry assembled!\n\n");
    }

    public void printJewelryTotalCostAndWeight() {                           // Method to print total cost and weight of a jewelry
        double totalCost = 0.0;
        double totalWeight = 0.0;
        for (Gem gem : jewelryGems) {
            totalCost += gem.getCost();
            totalWeight += gem.getWeight();
        }
        log.append("Total cost of a jewelry: ")
                .append(totalCost)
                .append(" $\n");
        log.append("Total weight of a jewelry: ")
                .append(totalWeight)
                .append(" ct\n\n");
        System.out.println("Total cost of a jewelry: " + totalCost + " $");
        System.out.println("Total weight of a jewelry: " + totalWeight + " ct");
        System.out.println();
    }

    public void sortJewelryGemsBy(ComparatorCriterion criterion) {          // Sorting method
        switch (criterion) {
            case TYPE:
                jewelryGems.sort(Comparator.comparing(Gem::getType));
                break;
            case COST:
                jewelryGems.sort(Comparator.comparingDouble(Gem::getCost));
                break;
            case CLARITY:
                jewelryGems.sort(Comparator.comparingDouble(Gem::getClarity));
                break;
            case WEIGHT:
                jewelryGems.sort(Comparator.comparingDouble(Gem::getWeight));
                break;
            case COLOR:
                jewelryGems.sort(Comparator.comparing(Gem::getColor));
                break;
            default:
                log.append("Illegal sorting criterion!\n\n");
                throw new IllegalArgumentException("Illegal sorting criterion!");
        }
        log.append("Sorting jewelry gems by criterion: ")
                .append(criterion)
                .append("\n\n");
    }

    public void printGemWithNumericalParameter (ComparatorCriterion criterion, double rangeMinValue, double rangeMaxValue) {  // Method to find gem(s) with specific parameters
        List<Gem> toReturn = new ArrayList<>();
        if (rangeMinValue <= rangeMaxValue) {
            for (Gem gem : jewelryGems) {
                double parameterToCompare;
                switch (criterion) {
                    case COST:
                        parameterToCompare = gem.getCost();
                        break;
                    case WEIGHT:
                        parameterToCompare = gem.getWeight();
                        break;
                    case CLARITY:
                        parameterToCompare = gem.getClarity();
                        break;
                    default:
                        throw new IllegalArgumentException("Illegal criterion!");
                }
                if (parameterToCompare > rangeMinValue && parameterToCompare < rangeMaxValue) {
                    toReturn.add(gem);
                }
            }
            if (toReturn.isEmpty()) {
                log.append("Unfortunately, there is no gem with necessary parameter!")
                   .append("\n\n");
                System.out.println("Unfortunately, there is no sweet with necessary parameter!");
            } else {
                log.append("There is(are) ")
                        .append(toReturn.size())
                        .append(" gem(s) that has(ve) necessary parameter ")
                        .append(criterion)
                        .append(" (")
                        .append(rangeMinValue)
                        .append(" - ")
                        .append(rangeMaxValue)
                        .append("):\n");
                System.out.println("There is(are) " + toReturn.size() + " gem(s) that has(ve) necessary parameter "+ criterion + " (" + rangeMinValue + " - " + rangeMaxValue + "):");
                printJewelryGems(toReturn);
            }
        } else {
            log.append("Minimum range value cannot be bigger than maximum range value!")
               .append("\n\n");
            throw new IllegalArgumentException("Minimum range value cannot be bigger than maximum range value!");
        }
    }

    public void printJewelryGems(List<Gem> jewelryGems) {       // Printing method
        for (Gem gem : jewelryGems) {
            System.out.println(gem);
            log.append(gem)
               .append("\n");
        }
        System.out.println();
        log.append("\n");
    }
}
