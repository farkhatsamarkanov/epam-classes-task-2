package com.epam.javaclasses.entity;

public class Emerald extends Gem {
    public Emerald(String type, double cost, double weight, double clarity, String color) {
        super(type, cost, weight, clarity, color);
    }

    @Override
    public String cut() {
        return "Emerald is being cut";
    }
}
