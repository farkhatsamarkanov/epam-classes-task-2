package com.epam.javaclasses.logger;

import com.epam.javaclasses.entity.Jewelry;
import java.io.*;

public class LogWriter {            // Class to write log to a file
    public static void writeJewelryAssemblingLog(String fileName, Jewelry jewelry) {
        File file = new File(fileName);
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        PrintWriter printWriter = null;
        try{
            if (file.createNewFile()) {
                System.out.println("Log file created!");
            } else {
                System.out.println("Log file already exists!");
            }
            fileWriter = new FileWriter(file, true);
            bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
            printWriter.println(jewelry.getLog());
            System.out.println("Log is written!");
        }catch (IOException e){
            System.err.println("Error while opening file: " + e);
        }finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }
}
